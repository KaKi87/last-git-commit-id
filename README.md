# `last-git-commit-id`

Asynchronously or synchronously get the last Git commit ID.

## Usage

### Asynchronous

```js
import {
    getLastGitCommitId
} from 'last-git-commit-id';
(async () => {
    console.log(await getLastGitCommitId());
})();
```

### Synchronous

```js
import {
    getLastGitCommitIdSync
} from 'last-git-commit-id';
console.log(getLastGitCommitIdSync());
```

Inspired by answers to the [Get hash of most recent git commit in Node](https://stackoverflow.com/questions/34518389/get-hash-of-most-recent-git-commit-in-node) question on StackOverflow, but using the newer & simpler syntax.