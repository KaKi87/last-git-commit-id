import {
    join as joinPath
} from 'node:path';
import {
    readFile
} from 'node:fs/promises';
import {
    readFileSync
} from 'node:fs';

const
    gitPath = joinPath(process.cwd(), '.git'),
    headPath = joinPath(gitPath, 'HEAD'),
    getRefPath = head => head.startsWith('ref: ') ? joinPath(gitPath, head.slice(5, -1)) : undefined;

export const
    /**
     * Asynchronously get the last Git commit ID.
     * @returns {Promise<string>}
     */
    getLastGitCommitId = async () => {
        const
            head = await readFile(headPath, 'utf8'),
            refPath = getRefPath(head);
        return (refPath ? await readFile(refPath, 'utf8') : head).trim();
    },
    /**
     * Synchronously get the last Git commit ID.
     * @returns {string}
     */
    getLastGitCommitIdSync = () => {
        const
            head = readFileSync(headPath, 'utf8'),
            refPath = getRefPath(head);
        return (refPath ? readFileSync(refPath, 'utf8') : head).trim();
    };